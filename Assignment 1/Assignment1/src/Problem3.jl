### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ ad392c52-af28-11eb-36ef-a9ff3752d809
using Pkg

# ╔═╡ cb65a140-af28-11eb-382b-43571fe566dd
using PlutoUI

# ╔═╡ d23d56c0-af28-11eb-2175-7d9b45acf993
md"# Assignment 1 - Problem 3"

# ╔═╡ c441a080-af28-11eb-332c-e3e655615bcd
Pkg.activate("Project.toml")

# ╔═╡ f178f1be-af28-11eb-01a7-132abf4a44ab
struct Elements
	name::String
	value::Int64
	forbiddenValues::Vector{Int64}
end

# ╔═╡ 37641610-b433-11eb-2b9c-0b07d82be532
begin
	
	x1 = Elements("x1", 0, [1])
	x2 = Elements("x2", 0, [0])
	x3 = Elements("x3", 1, [0])
	x4 = Elements("x4", 0, [1])
	x5 = Elements("x5", 0, [0])
	x6 = Elements("x6", 0, [0])
	x7 = Elements("x7", 0, [0])

end

# ╔═╡ 674c3a00-b434-11eb-349e-d7fa3e4a319d
	elArray = [x1, x2, x3, x4, x5, x6, x7]


# ╔═╡ c498c490-b433-11eb-084d-d3e8b0672062
md"## Naive Inference"

# ╔═╡ a15edd60-b434-11eb-1502-b1bd173fc30d


# ╔═╡ 11d80180-b434-11eb-0d71-4b9706fc1fd3
conn = Dict(
	x3 => [(x1.value, x1), (x4.value, x4)], 
	x1 => [(x3.value, x3), (x2.value, x2), (x4.value, x4), (x5.value, x5), (x6.value, x6)], 
	x2 => [(x1.value, x1), (x5.value, x5)],
	x4 => [(x1.value, x1), (x2.value, x2), (x3.value, x3), (x5.value, x5)],
	x5 => [(x1.value, x1), (x2.value, x2), (x4.value, x4), (x6.value, x6)],
	x6 => [(x4.value, x4), (x1.value, x1), (x5.value, x5),(x7.value, x7)],
	x7 => [(x6.value, x6)]
)

# ╔═╡ c1e0f790-b433-11eb-1a95-7fdb1e261ddb
function connections(element::Elements)
	
	
	
	
end

# ╔═╡ f5626450-b433-11eb-0916-8f28e9531841
connections(x3)

# ╔═╡ Cell order:
# ╠═d23d56c0-af28-11eb-2175-7d9b45acf993
# ╠═ad392c52-af28-11eb-36ef-a9ff3752d809
# ╠═c441a080-af28-11eb-332c-e3e655615bcd
# ╠═cb65a140-af28-11eb-382b-43571fe566dd
# ╠═f178f1be-af28-11eb-01a7-132abf4a44ab
# ╠═37641610-b433-11eb-2b9c-0b07d82be532
# ╠═674c3a00-b434-11eb-349e-d7fa3e4a319d
# ╟─c498c490-b433-11eb-084d-d3e8b0672062
# ╠═a15edd60-b434-11eb-1502-b1bd173fc30d
# ╠═11d80180-b434-11eb-0d71-4b9706fc1fd3
# ╠═c1e0f790-b433-11eb-1a95-7fdb1e261ddb
# ╠═f5626450-b433-11eb-0916-8f28e9531841

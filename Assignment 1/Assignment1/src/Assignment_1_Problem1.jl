### A Pluto.jl notebook ###
# v0.12.21

using Markdown
using InteractiveUtils

# ╔═╡ ce46e9e6-b3a6-11eb-123b-ff8e98bd864f
using Pkg,PlutoUI

# ╔═╡ 2e9d6a30-b425-11eb-15f5-39b585a90761
md"# Assignmnet 1 - Problem 1"

# ╔═╡ ce0ef215-fbb9-4abc-8830-4aa9c3cb2611
mutable struct State 
	s1::Bool  
	s2::Vector{Bool} 
	s3::Vector{Bool}   
	s4::Bool 
	position::Int64   
end

# ╔═╡ 99617c50-b423-11eb-39f3-93a7303d3673
struct Action
	name::String
	cost::Int64
end

# ╔═╡ 9f12b330-b423-11eb-3b52-5b262dd94ea1
begin
	mEast = Action("move east", 3)
	mWest = Action("move west", 3)
	collect = Action("collect", 5)
	remain = Action("remain", 1)
end

# ╔═╡ a0f5a400-b423-11eb-0c4b-611d0fb154b7
md"## Movement Functions"

# ╔═╡ de689625-ad65-4823-bb33-1b90c10fc0a8
function MoveEast(ln::State)
	NewState=deepcopy(ln)
	
	if ln.position==1
		NewState.position=1
	elseif ln.position==2
		NewState.position=1
	elseif ln.position==3
		NewState.position=2
	else
		NewState.position=3
	end
		return NewState
end

# ╔═╡ 952b2509-f855-4c90-9f01-ab2d5ac06fd9
function MoveWest(ln::State)
	NewState=deepcopy(ln)
	
	if ln.position==1
		NewState.position=2
	elseif ln.position==2
		NewState.position=3
	elseif ln.position==3
		NewState.position=4
	else
		NewState.position=4
	end
	
		return NewState
end

# ╔═╡ 8c0093aa-ee50-4a0e-b54e-395b6a8ee459
function CollectItem(ln::State)
	NewState=deepcopy(ln)
	
	if ln.position==1
		if ln.s1==true
			NewState.s1=false
		end
		
	elseif ln.position==2
			if ln.s2[1]==true
				NewState.s2[1]=false
			elseif ln.s2[2]==true
					NewState.s2[2]=false
			else
				NewState.s2[3]=false
			end
	elseif ln.position==3	
		if ln.s3[1]==true
			NewState.s3[1]=false
		else
			NewState.s3[2]=false
		end
	else
		if ln.s4==true
			NewState.s4=false
		end
	end
		return NewState
end

# ╔═╡ 0c32f6e6-e899-4089-ae48-bc5b2f959b7c
startNode=State(true,[true,true,true],[true,true],true,2)

# ╔═╡ d26e51fe-266f-4014-b4f0-5d073f1a1744
Generatedstates=Vector{State}()

# ╔═╡ f4d73699-f56b-48e8-af35-c8df19ffcb72
push!(Generatedstates,startNode)

# ╔═╡ 7ee9b0b5-8711-4aaa-90c2-79101495dc82
# push!(Generatedstates,node)
empty!(Generatedstates)
# Generatedstates

# ╔═╡ e038d70b-1965-4759-983a-627bd55bcce9
with_terminal() do
	println(Generatedstates)
end

# ╔═╡ 81ecc6e0-b425-11eb-0c64-db1a1f280570
length(Generatedstates)

# ╔═╡ 0f0a3950-b425-11eb-33d9-e5575e51fb36
md"# Function to Generate States"

# ╔═╡ 82176ac3-d1d8-4b51-92cd-a05b32e2d67a
function generateStates(stateparameter::State)
	
	stategenerated=deepcopy(stateparameter)
	
	collectBool=false
	for i in 1:length(Generatedstates)
		if ("$(CollectItem(stategenerated))"=="$(Generatedstates[i])")
						collectBool=true	
		end
	end
   if collectBool==false
		  push!(Generatedstates,CollectItem(stategenerated))
		  
		collectGenerated=deepcopy(last(Generatedstates))
		
   end

	MoveEastBool=false
	for i in 1:length(Generatedstates)
		if ("$(MoveEast(stategenerated))"=="$(Generatedstates[i])")
						MoveEastBool=true	
		end
	end
   if MoveEastBool==false
		  push!(Generatedstates,MoveEast(stategenerated))
		MoveEastGenerated=deepcopy(last(Generatedstates))
   end
	
	MoveWestBool=false
	for i in 1:length(Generatedstates)
		if ("$(MoveWest(stategenerated))"=="$(Generatedstates[i])")
						MoveWestBool=true	
		end
	end
   if MoveWestBool==false
		  push!(Generatedstates,MoveWest(stategenerated))
		MoveWestGenerated=deepcopy(last(Generatedstates))
   end
		
		if length(Generatedstates)<10
	  		generateStates(collectGenerated)  
	    end
	        
		# stategenerated=last(Generatedstates)
		# if length(Generatedstates)>512
		# 	break
		# end 	

end

# ╔═╡ 0df33b1e-1019-4306-8124-888bc64fda19
generateStates(startNode)

# ╔═╡ ba86d400-b425-11eb-30fb-33d58518daac
md"#### Number of items in rooms 2 and 3"

# ╔═╡ d5a138e0-b423-11eb-24c0-2bb88c5cad96
function hValSquareTwo(stateNode::State)
h = 0
	# for i in 1:stateNode.s2
		if stateNode.s2[1] == true
			h += 1
		else
			h += 0
		end
	
		if stateNode.s2[2] == true
			h += 1
		else
			h += 0
		end	
	
		if stateNode.s2[3] == true
			h += 1
		else
			h += 0
		end
	# end
end

# ╔═╡ 1b709efe-b424-11eb-3433-9deaf38563ce
function hValSquareThree(stateNode::State)
h = 0
	# for i in 1:stateNode.s2
		if stateNode.s3[1] == true
			h += 1
		else
			h += 0
		end
	
		if stateNode.s3[2] == true
			h += 1
		else
			h += 0
		end	
	
		
	# end
end

# ╔═╡ 4b88c910-b424-11eb-1acf-41aef3bd402f
md"# Transition Model"

# ╔═╡ 20b7a260-b424-11eb-1c9c-bf30172834d7
transModel = Dict()

# ╔═╡ 2ccd3380-b424-11eb-1557-4d192737045a
function transMod(states::Vector{State})	
	
	for i in 1:length(states)
		
	key = states[i]
		
		#position 1
		if states[i].position == 1
			if states[i].s1 == true
				#collect + move west + remain (move east)
				value = [(mEast, MoveEast(states[i])), (mWest, MoveWest(states[i])), (collect, CollectItem(states[i]))]
				# if "$key" != "($last(Generatedstates))"
					# model = Dict(zip(key, value))
					push!(transModel, key => value)
					merge(transModel)
				# end

			else #move west + remain (move east)
				value = [(mWest, MoveWest(states[i])), (mEast, MoveWest(states[i]))]
				# if "$key" != "($last(Generatedstates))"
					# model = Dict(zip(key, value))
					push!(transModel, key => value)

					merge(transModel)
				# end
			end
		
		#position 2
		elseif states[i].position == 2
			for sTwo in 1:length(states[i].s2) #!!!!CHECK THE ITERATORS
				# if (states[i].squareTwo[1] == false && states[i].squareTwo[1] == false && states[i].squareTwo[1] == false)
				if (hValSquareTwo(states[i]) >= 1)
					#move east + west and collect
				value = [(mEast, MoveEast(states[i])), (mWest, MoveWest(states[i])), (collect, CollectItem(states[i]))]
					# if "$key" != "($last(Generatedstates))"
						# model = Dict(zip(key, value))
						push!(transModel, key => value)
						merge(transModel)
					# end
				else #move east + west 
				value = [(mWest, MoveWest(states[i])), (mEast, MoveWest(states[i]))]
					# if "$key" != "($last(Generatedstates))"
						# model = Dict(zip(key, value))
						push!(transModel, key => value)
						merge(transModel)
					# end
					
				end
			end
		

		#position 3
		elseif states[i].position == 3
			for sThree in 1:length(states[i].s3)
				if (hValSquareThree(states[i]) >= 1)
					#move east + west and collect
				value = [(mEast, MoveEast(states[i])), (mWest, MoveWest(states[i])), (collect, CollectItem(states[i]))]
					# if "$key" != "($last(Generatedstates))"
						# model = Dict(zip(key, value))
						push!(transModel, key => value)
						merge(transModel)
					# end
				else
					#move east + west 
				value = [(mWest, MoveWest(states[i])), (mEast, MoveWest(states[i]))]
					# if "$key" != "($last(Generatedstates))"
						# model = Dict(zip(key, value))
						push!(transModel, key => value)
						merge(transModel)
					# end
				end
			end
			

		#position 4
		else	
			if states[i].s4 == true
				#collect + move west + remain (move east)
				value = [(mEast, MoveEast(states[i])), (mWest, MoveWest(states[i])), (collect, CollectItem(states[i]))]
				# if "$key" != "($last(Generatedstates))"
					# model = Dict(zip(key, value))
					push!(transModel, key => value)
					merge(transModel)
				# end

			else #move west + remain (move east)
				value = [(mWest, MoveWest(states[i])), (mEast, MoveWest(states[i]))]
				# if "$key" != "($last(Generatedstates))"
					# model = Dict(zip(key, value))
					push!(transModel, key => value)
					merge(transModel)
				# end
			end
			
		end
	end
end

# ╔═╡ 31d24be0-b424-11eb-184c-d52004ae1b81
transMod(Generatedstates)

# ╔═╡ ea5392a0-b424-11eb-0e44-a9a565502247
md"## Calculate Heuristic"

# ╔═╡ d4dba980-b424-11eb-0879-23b68f6f0e47
function calcHVal(stateNode)
	h = 0
	
	if stateNode.s1 == true
		h += 1
	
	else
		h += 0
			
	end
	
	# for i in stateNode.squareTwo
		if stateNode.s2[1] == true
			h += 1
		else
			h += 0
		end
	
		if stateNode.s2[2] == true
			h += 1
		else
			h += 0
		end	
	
		if stateNode.s2[3] == true
			h += 1
		else
			h += 0
		end
	# end
	
	# for j in stateNode.squareThree
	
		if stateNode.s3[1] == true
			h += 1
		else
			h += 0
		end
	
		if stateNode.s3[2] == true
			h += 1
		else
			h += 0
		end
	
		# if stateNode.squareThree[3] == true
		# 	h += 1
		# else
		# 	h += 0
		# end
	# end
		
	if stateNode.s4 == true
		h += 1
	
	else
		h += 0
			
	end
	
	return h
end

# ╔═╡ 3e988f60-b424-11eb-1143-9daea01d0e75
md"## A* Algorithm"

# ╔═╡ 7f7568d0-b426-11eb-2f63-e9f86ab19949
begin
	 openSet =Vector{Tuple}()
	 closedSet =Vector{Tuple}()
	 
end

# ╔═╡ 765275b0-b424-11eb-1040-73ea0badd7b5
function aStar(startNode::State)
	
	# lowestCost = 0
	generatedNodes = []
	
	push!(openSet,(startNode,calcHVal(startNode)))
	
	currentNode = last(openSet)[1]
	
	#state
	 statesPath =Vector{Tuple}()
	
	totalCost = 0
	
	currentLowestState = startNode
	
	while !isempty(openSet)
		# compare the costs and get the lowest cost
		compLowCost = 1
		 # should be declared outsidewhile loop, cause it will be created everytime the while start again
		
		# check if currentNode is a goal state
		if calcHVal(currentNode) == 0
			with_terminal() do
				println("Path taken: $statesPath")				
			end
			break
		end
		
		# generate the successor nodes --  transition model
		empty!(generatedNodes)
		
		for i in 1:length(transModel[currentNode])
			push!(generatedNodes, transModel[currentNode][i])
			
		end
		
		
		#algorithm starts here
		
		for j in 1:length(generatedNodes)
			
			#check if generated node is goal state
			#will generatedNodes[i] print only print the state or should we add [1]
			if calcHVal(generatedNodes[i]) == 0
				push!(statesPath,(generatedNodes[i][2],generatedNodes[i][1].cost,calcHVal(generatedNodes[i])))
				with_terminal() do
				 println("Path taken: $statesPath")				
			   end
				break
			end
			
			# comparing the generated nodes to find the node with the lowest cost
			#the operator should be <= and not just < =, cause compLowCost can be 5
			if generatedNodes[i][1].cost >= compLowCost 
				
				isempty!(currentLowestState)
				currentLowestState = generatedNodes[i][2]					
				compLowCost = generatedNodes[i][1].cost
			end
			
			#check if each node successor is in the open list
				if (generatedNodes[i][2],calcHVal(generatedNodes[i][2])) in openSet
					if generatedNodes[i][1].cost<=totalCost
							compLowCost = 1# cause if the next generatedNodes[i][1].cost  be great than previous, this condition (if generatedNodes[i][1].cost <= compLowCost) will not be accessed anymore.
								continue
					end
				elseif (generatedNodes[i][2],calcHVal(generatedNodes[i][2])) in closedSet
					if generatedNodes[i][1].cost<=totalCost
						       #move nodesuccessor from the closed list to the open list, delete the Node (generatedNodes[i][1]) from closelist nd add to open list
					#(and add in the openlist)means and also compare the move cost wiht the other to check if it will be the current node in case its movecost has the lowest cost-->this part will be done cause it will loop 
					# There is no need to add in the openlist it's already there so we just remove from the closed list
					#delete element from any array
					#findfirst to the position of an specific element from the array
					#to delete element from array
					splice!(closedSet, findfirst(==((generatedNodes[i][1],calcHVal(generatedNodes[i][1]))),closedSet))
					           continue
				
				    end
				
			    else
					push!(openSet,(generatedNodes[i][2],calcHVal(generatedNodes[i][2])))
				end
			
		end #END OF FOR LOOP
				
		currentCost = compLowCost
		totalCost += currentCost
        push!(statesPath,(currentLowestState,compLowCost,calcHVal(currentLowestState)))
		
		push!(closedSet,curentNode,calcHVal(curentNode))
		
		currentNode = currentLowestState

	end
	
end



# ╔═╡ e73f5850-b425-11eb-3bd7-c761416875e7
aStar(startNode)

# ╔═╡ ee124850-b424-11eb-3d96-c1fce91ed3c5


# ╔═╡ Cell order:
# ╟─2e9d6a30-b425-11eb-15f5-39b585a90761
# ╠═ce46e9e6-b3a6-11eb-123b-ff8e98bd864f
# ╠═ce0ef215-fbb9-4abc-8830-4aa9c3cb2611
# ╠═99617c50-b423-11eb-39f3-93a7303d3673
# ╠═9f12b330-b423-11eb-3b52-5b262dd94ea1
# ╟─a0f5a400-b423-11eb-0c4b-611d0fb154b7
# ╠═de689625-ad65-4823-bb33-1b90c10fc0a8
# ╠═952b2509-f855-4c90-9f01-ab2d5ac06fd9
# ╠═8c0093aa-ee50-4a0e-b54e-395b6a8ee459
# ╠═0c32f6e6-e899-4089-ae48-bc5b2f959b7c
# ╠═d26e51fe-266f-4014-b4f0-5d073f1a1744
# ╠═f4d73699-f56b-48e8-af35-c8df19ffcb72
# ╠═7ee9b0b5-8711-4aaa-90c2-79101495dc82
# ╠═e038d70b-1965-4759-983a-627bd55bcce9
# ╠═0df33b1e-1019-4306-8124-888bc64fda19
# ╠═81ecc6e0-b425-11eb-0c64-db1a1f280570
# ╟─0f0a3950-b425-11eb-33d9-e5575e51fb36
# ╠═82176ac3-d1d8-4b51-92cd-a05b32e2d67a
# ╟─ba86d400-b425-11eb-30fb-33d58518daac
# ╠═d5a138e0-b423-11eb-24c0-2bb88c5cad96
# ╠═1b709efe-b424-11eb-3433-9deaf38563ce
# ╟─4b88c910-b424-11eb-1acf-41aef3bd402f
# ╠═20b7a260-b424-11eb-1c9c-bf30172834d7
# ╠═2ccd3380-b424-11eb-1557-4d192737045a
# ╠═31d24be0-b424-11eb-184c-d52004ae1b81
# ╟─ea5392a0-b424-11eb-0e44-a9a565502247
# ╠═d4dba980-b424-11eb-0879-23b68f6f0e47
# ╟─3e988f60-b424-11eb-1143-9daea01d0e75
# ╠═7f7568d0-b426-11eb-2f63-e9f86ab19949
# ╠═765275b0-b424-11eb-1040-73ea0badd7b5
# ╠═e73f5850-b425-11eb-3bd7-c761416875e7
# ╠═ee124850-b424-11eb-3d96-c1fce91ed3c5
